package id.ac.ui.cs.mobileprogramming.muhammadafkar.easysplitbill.utils;

public class Constant {
    public static final int ONE_DAY_TIME_IN_MILLIS = 86400000;
    public static final int ONE_HOUR_IN_MILLIS = 3600000;
}
